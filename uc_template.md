# 1 Use-Case Name
*Template*

## 1.1 Brief Description
 *Description*

# 2 Flow of Events
## 2.1 Basic Flow
*Flow*

### 2.1.1 Activity Diagram
![Title](url)

### 2.1.2 Mock-up
![Title](url)

### 2.1.3 Narrative
(n/a)

## 2.2 Alternative Flows
(n/a)

# 3 Special Requirements
(n/a)

# 4 Preconditions
## 4.1 e.g. Login

# 5 Postconditions
(n/a)
 
# 6 Extension Points
(n/a)
